from django.contrib import admin
from . models import User, Workspace, Group, Task, Label
# Register your models here.

admin.site.register(User),
admin.site.register(Workspace),
admin.site.register(Group),
admin.site.register(Task),
admin.site.register(Label),