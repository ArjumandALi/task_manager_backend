# Generated by Django 4.1.4 on 2022-12-20 09:54

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('taskmangerapp', '0003_rename_workspace_id_group_workspace_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='task',
            name='task_status',
            field=models.CharField(default=False, max_length=50),
            preserve_default=False,
        ),
        migrations.CreateModel(
            name='Label',
            fields=[
                ('label_id', models.IntegerField(auto_created=True, editable=False, primary_key=True, serialize=False)),
                ('label_status', models.CharField(max_length=50)),
                ('task_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='taskmangerapp.task')),
            ],
        ),
    ]
