# Generated by Django 4.1.4 on 2022-12-20 12:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('taskmangerapp', '0008_alter_user_user_role'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='user_role',
            field=models.CharField(choices=[('AD', 'Admin'), ('ER', 'Employer'), ('EE', 'Employee'), ('TL', 'Team Lead'), ('SL', 'Sub Lead'), ('PM', 'Project Manger')], default='ER', max_length=2),
        ),
    ]
