from django.db import models
from django.contrib.auth.models import User

# Create your models here.


class User(models.Model):
    ADMIN = 'AD'
    EMPLOYER = 'ER'
    EMPLOYEE = 'EE'
    TEAM_LEADS = 'TL'
    SUB_LEADS = 'SL'
    PROJECT_MANGER = 'PM'
    USER_ROLE_CHOICES = [
        (ADMIN, 'Admin'),
        (EMPLOYER, 'Employer'),
        (EMPLOYEE, 'Employee'),
        (TEAM_LEADS, 'Team Lead'),
        (SUB_LEADS, 'Sub Lead'),
        (PROJECT_MANGER, 'Project Manger'),
    ]
    id = models.AutoField(primary_key=True)
    user_name = models.CharField(max_length=50)
    user_role = models.CharField(
        max_length=2, choices=USER_ROLE_CHOICES, default=EMPLOYER)
    user_email = models.EmailField(max_length=100)
    user_address = models.CharField(max_length=100)

    def __str__(self):
        return self.user_name


class Workspace(models.Model):
    id = models.AutoField(primary_key=True)
    ws_title = models.CharField(max_length=50)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.ws_title


class Group(models.Model):
    id = models.AutoField(primary_key=True)
    group_title = models.CharField(max_length=50)
    workspace = models.ForeignKey(Workspace, on_delete=models.CASCADE)

    def __str__(self):
        return self.group_title


class Task(models.Model):
    URGENT = 'UR'
    HIGH = 'HG'
    NORMAL = 'NL'
    LOW = 'LW'
    TASK_STATUS_CHOICES = [
        (URGENT, 'Urgent'),
        (HIGH, 'High'),
        (NORMAL, 'Normal'),
        (LOW, 'Low')
    ]
    id = models.AutoField(primary_key=True)
    task_title = models.CharField(max_length=100)
    task_subtitle = models.CharField(max_length=100)
    task_desc = models.CharField(max_length=200)
    task_status = models.CharField(
        max_length=2, choices=TASK_STATUS_CHOICES, default=NORMAL)
    group = models.ForeignKey(
        Group, on_delete=models.CASCADE)

    def __str__(self):
        return self.task_title


class Label(models.Model):
    OPEN = 'OP'
    IN_PROGRESS = 'IP'
    DONE = 'DN'
    CLOSED = 'CL'
    LABEL_CHOICES = [
        (OPEN, 'Open'),
        (IN_PROGRESS, 'In Progress'),
        (DONE, 'Done'),
        (CLOSED, 'Closed'),
    ]
    id = models.AutoField(primary_key=True)
    task_name = models.ForeignKey(
        Task, on_delete=models.CASCADE)
    label_status = models.CharField(
        max_length=2, choices=LABEL_CHOICES, default=OPEN)

    def __str__(self):
        return self.label_status
