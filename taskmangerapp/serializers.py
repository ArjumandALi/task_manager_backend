# import serializer from rest_framework
from rest_framework import serializers
# import model from models.py
from .models import Workspace, Group, Task, Label


# coderarts
from django.contrib.auth.models import User
from rest_framework.response import Response
from rest_framework import status
from rest_framework.validators import UniqueValidator
from django.contrib.auth.password_validation import validate_password

# Create a model serializer


# class UserSerializer(serializers.ModelSerializer):
#     # specify model and fields
#     class Meta:
#         model = User
#         fields = ('id', 'user_name', 'user_role',
#                   'user_email', 'user_address')

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ["id", "first_name", "last_name", "username"]


class WorkspaceSerializer(serializers.ModelSerializer):
    # specify model and fields
    class Meta:
        model = Workspace
        fields = ('id', 'ws_title', 'user')


class GroupSerializer(serializers.ModelSerializer):
    # specify model and fields
    class Meta:
        model = Group
        fields = ('id', 'group_title', 'workspace')


class TaskSerializer(serializers.ModelSerializer):
    # specify model and fields
    class Meta:
        model = Task
        fields = ('id', 'task_title',
                  'task_subtitle', 'task_desc', 'group')


class LabelSerializer(serializers.ModelSerializer):
    # specify model and fields
    class Meta:
        model = Label
        fields = ('id', 'label_status',
                  'task_name')


# Serializer to Register User


class RegisterSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(required=True, validators=[UniqueValidator(queryset=User.objects.all())])
    password = serializers.CharField(write_only=True, required=True, validators=[validate_password])
    password2 = serializers.CharField(write_only=True, required=True)

    class Meta:
        model = User
        fields = ('username', 'password', 'password2',
                  'email', 'first_name', 'last_name')
        extra_kwargs = {
            'first_name': {'required': True},
            'last_name': {'required': True}
        }

    def validate(self, attrs):
        if attrs['password'] != attrs['password2']:
            raise serializers.ValidationError(
                {"password": "Password fields didn't match."})
        return attrs

    def create(self, validated_data):
        user = User.objects.create(
            username=validated_data['username'],
            email=validated_data['email'],
            first_name=validated_data['first_name'],
            last_name=validated_data['last_name']
        )
        user.set_password(validated_data['password'])
        user.save()
        return user
