# basic URL Configurations
from django.urls import include, path
# import routers
from rest_framework import routers
from . import views
# import everything from views
from .views import *

# define the router
router = routers.DefaultRouter()

# define the router path and viewset to be used
router.register(r'workspaces', WorkspaceViewSet)
router.register(r'groups', GroupViewSet)
router.register(r'tasks', TaskViewSet)
router.register(r'users', UserViewSet)
router.register(r'labels', LabelViewSet)

# specify URL Path for rest_framework
urlpatterns = [
    path('', include(router.urls)),
    path('taskmangerapp-auth/', include('rest_framework.urls')),
    path("get-details", UserDetailAPI.as_view()),
    path('register', RegisterUserAPIView.as_view()),
]

