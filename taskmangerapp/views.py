from django.shortcuts import render
from . models import User, Workspace, Group, Task, Label
from rest_framework import viewsets
from .serializers import UserSerializer, WorkspaceSerializer, GroupSerializer, TaskSerializer, LabelSerializer


from rest_framework.permissions import AllowAny
from rest_framework.views import APIView
from rest_framework.response import Response
from .serializers import UserSerializer, RegisterSerializer
from django.contrib.auth.models import User
from rest_framework.authentication import TokenAuthentication
from rest_framework import generics

from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated

# Create your views here.

#if (User.is_authenticated):
class UserViewSet(viewsets.ModelViewSet):
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated]
    # define queryset
    queryset = User.objects.all()

    # specify serializer to be used
    serializer_class = UserSerializer

if (User.is_authenticated):
    class WorkspaceViewSet(viewsets.ModelViewSet):
        authentication_classes = [SessionAuthentication, BasicAuthentication]
        permission_classes = [IsAuthenticated]
        # define queryset
        queryset = Workspace.objects.all()

        # specify serializer to be used
        serializer_class = WorkspaceSerializer


if (User.is_authenticated):
    class GroupViewSet(viewsets.ModelViewSet):
        authentication_classes = [SessionAuthentication, BasicAuthentication]
        permission_classes = [IsAuthenticated]
        # define queryset
        queryset = Group.objects.all()

        # specify serializer to be used
        serializer_class = GroupSerializer


if (User.is_authenticated):
    class TaskViewSet(viewsets.ModelViewSet):
        authentication_classes = [SessionAuthentication, BasicAuthentication]
        permission_classes = [IsAuthenticated]
        # define queryset
        queryset = Task.objects.all()

        # specify serializer to be used
        serializer_class = TaskSerializer


if (User.is_authenticated):
    class LabelViewSet(viewsets.ModelViewSet):
        authentication_classes = [SessionAuthentication, BasicAuthentication]
        permission_classes = [IsAuthenticated]

        queryset = Label.objects.all()
        serializer_class = LabelSerializer


# Class based view to Get User Details using Token Authentication
if (User.is_authenticated):
    class UserDetailAPI(APIView):
        authentication_classes = (TokenAuthentication,)
        permission_classes = (AllowAny,)

        def get(self, request, *args, **kwargs):
            user = User.objects.get(id=request.user.id)
            serializer = UserSerializer(user)
            return Response(serializer.data)

# Class based view to register user


if (User.is_authenticated):
    class RegisterUserAPIView(generics.CreateAPIView):
        permission_classes = (AllowAny,)
        serializer_class = RegisterSerializer
